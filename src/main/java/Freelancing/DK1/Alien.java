
 
package Freelancing.DK1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
//@Scope(value="prototype")
public class Alien {
	
private int aid;
private String aname;
private String tech;
@Autowired              // searching object by type in SPring COntainer
@Qualifier("laptop1")  // searching object by name in SPring COntainer
private Laptop laptop;// = new Laptop();

public int getAid() {
	return aid;
}


public void setAid(int aid) {
	this.aid = aid;
}


public String getAname() {
	return aname;
}


public void setAname(String aname) {
	this.aname = aname;
}


public String getTech() {
	return tech;
}


public void setTech(String tech) {
	this.tech = tech;
}
	
	public Alien() {
		System.out.println("autmatically Alien object creation when use @component in Spring COntainer in default constructor");
	}

	public void show() {
		System.out.println("show something");
		laptop.compile();
	}
	


}
