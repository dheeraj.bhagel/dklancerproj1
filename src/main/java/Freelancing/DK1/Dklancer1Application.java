package Freelancing.DK1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Dklancer1Application {

	
	public static void main(String[] args) {
		ConfigurableApplicationContext context= SpringApplication.run(Dklancer1Application.class, args);  //run(class<?>,String)
		/*When no bean created then spring Container automatically run default constructor of @component class 
		 * it only created only one object bcz of singleton  and made a many object we use prototype in @ scope*/
		
		
		/*
		Alien a1 =context.getBean(Alien.class);
		a1.show();
		
		Alien a2 =context.getBean(Alien.class); // only once be call default constructor when @ component is used without prototype
		a2.show();
		*/
		
		//System.out.println("welcome DKFreelancer");
		
		Alien a1 =context.getBean(Alien.class);
		a1.show();
		
	}

}
