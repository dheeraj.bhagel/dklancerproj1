package Freelancing.DK1;

import org.springframework.stereotype.Component;

@Component("laptop1")    // instantaneous object means Create new object with name laptop1
public class Laptop {
   String model;
   int laptopId;
   String brand;
   
	
	public String getModel() {
	return model;
}

public void setModel(String model) {
	this.model = model;
}

public int getLaptopId() {
	return laptopId;
}

public void setLaptopId(int laptopId) {
	this.laptopId = laptopId;
}

public String getBrand() {
	return brand;
}

public void setBrand(String brand) {
	this.brand = brand;
}


	@Override
public String toString() {
	return "Laptop [model=" + model + ", laptopId=" + laptopId + ", brand=" + brand + "]";
}

	public Laptop() {
		System.out.println("Default Laptop constructor Created");
	}

	public void compile() {
		System.out.println("Compile Laptop class");
	}
	

}
